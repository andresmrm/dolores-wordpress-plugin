help:
	@echo "See Makefile or documentation in https://gitlab.com/andresmrm/dolores-wordpress-plugin"

clean:
	rm -rf build/
	rm -rf dist/

clean-all: clean clean-dev

clean-dev:
	rm -rf bower_components/
	rm -rf node_modules/
	rm -rf composer.lock vendor/

dev:
	grunt dev

deploy-sefn:
	script/deploy.sh sefn

deploy-sggpe:
	script/deploy.sh sggpe

install-dev: bower.json package.json
	npm install
	bower install
	composer install

prod:
	grunt prod
