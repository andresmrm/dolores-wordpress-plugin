"use strict";

var React = require("react");
var baseInputClass = require("./BaseInputClass.react");

var Input = React.createClass(baseInputClass);

module.exports = Input;
