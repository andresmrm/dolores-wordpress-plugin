"use strict";

var $ = require("jquery");
var React = require("react");
var baseInputClass = require("./BaseInputClass.react");

var baseSelectClass = $.extend({}, baseInputClass);

baseSelectClass.renderInput = function() {
    var onChange = function(e) {
      this.props.onChange(this.props.name, e.target.value);
    }.bind(this);

    var onFocus = function() {
      this.setState({focused: true});
    }.bind(this);

    var blur = function() {
      React.findDOMNode(this.refs.input).blur();
    }.bind(this);

    var options = $.map(window.cities, function (c) {
      return <option value={c}>{c}</option>
    })
    options.splice(0, 0, <option value="" selected="selected">Município</option>);
    if(window.dolores_out_of_place_option) {
      options.splice(1, 0, <option value={window.dolores_out_of_place_option}>{window.dolores_out_of_place_option}</option>);
    }

    return <select
        disabled={this.props.disabled}
        name={this.props.name}
        onChange={onChange}
        onFocus={onFocus}
        type={this.props.type}
        placeholder={this.props.placeholder}
        ref="input"
        value={this.props.value}>
          {options}
        </select>;
}

var Select = React.createClass(baseSelectClass);

module.exports = Select;
