"use strict";

var React = require("react");

var Lightbox = require("./Lightbox.react");

var Message = React.createClass({
  getInitialState: function() {
    return {
      show: false,
      text: '',
      title: '',
    };
  },

  hide: function() {
    this.setState(this.getInitialState());
  },

  show: function(data) {
    this.setState({show: true, text: data.message, title: data.title});
  },

  componentDidMount: function() {
    window.messageBox = this
  },

  render: function() {
    if (!this.state.show) {
      return null;
    }

    var title = this.state.title ? <span>{this.state.title}</span> : false

    var lightboxContent = (
      <div className="lightbox-content">
        <h2 className="signin-text">
          {title}
        </h2>
        <p className="signin-text">
          {this.state.text}
        </p>
      </div>
    );

    return (
      <Lightbox close={this.hide}>
        {lightboxContent}
      </Lightbox>
    );
  }
});

module.exports = Message;
