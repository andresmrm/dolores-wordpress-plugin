"use strict";

var async = require("./async");
var mapStyle = require("../sefn/mapstyle");

var gmapsKey = window.doloresConfig.gmapsKey;

var mapContainer;
var map = null;
var markers = [];
var geojson = [];
var clickCallback = null;
var contents = {};
var infoWindow = null;

// Colors for GeoJson Features
var geo_colors = {
  'médio': '#aa4133',
  'serrana': '#aa4133',

  'metropolitana': '#ae1801',
  'norte': '#ae1801',

  'baixadas': '#eb5e48',
  'centro': '#eb5e48',
  'costa': '#eb5e48',
  'noroeste': '#eb5e48',
}

window.initMap = function() {
  infoWindow = new window.google.maps.InfoWindow();
  clickCallback = function() {
    infoWindow.setContent(contents[this.getTitle()]);
    infoWindow.open(map, this);
  };

  map = new window.google.maps.Map(mapContainer, {
    center: {lat: -22.08, lng: -42.60},
    disableDefaultUI: true,
    scrollwheel: false,
    zoomControl: true,
    zoom: 8,
    styles: mapStyle,
  });

  map.data.setStyle(function(feature) {
    var name = feature.getProperty('name').toLowerCase()
    var color = '#eb5e48'
    for (var k in geo_colors) {
      if (name.indexOf(k) != -1) {
        color = geo_colors[k]
        break
      }
    }
    return {
      fillColor: color,
      strokeWeight: 0.5,
      strokeColor: color
    }
  });

  map.addListener("click", function(event) {
    infoWindow.close();
  });
  markers.map(window.addMapMarker);
  geojson.map(window.addGeoJson);

  map.data.addListener('click', function(event) {
    var name = event.feature.f.name
    var region = window.regions[name]
    var content = "" +
      "<div class=\"map-marker\">" +
      "<h3 class=\"map-marker-title\">" + name + "</h3>" +
      "<p class=\"map-marker-description\">" +
      "<strong>Usuários:</strong> " + region.user_count + "<br />" +
      "<strong>Ideias:</strong> " + region.post_count +
      "</p>" +
      "<p class=\"map-marker-description\">" +
      "<a class=\"map-marker-button\" href=\"" + region.link + "\">" +
      "Participar" +
      "</a>" +
      "</p>" +
      "</div>"

    infoWindow.setContent(content);
    infoWindow.setPosition(event.latLng);
    infoWindow.open(map);
  });
};

window.addMapMarker = function(data) {
  if (map === null) {
    markers.push(data);
  } else {
    var marker = new window.google.maps.Marker(data);
    marker.setMap(map);
    marker.addListener("click", clickCallback);
    contents[data.title] = data.content;
  }
};

window.addGeoJson = function(url) {
  if (map === null) {
    geojson.push(url);
  } else {
    map.data.loadGeoJson(url);
  }
}

var setup = function() {
  var params = "region=br&language=pt-BR&key=" + gmapsKey + "&callback=initMap";
  mapContainer = document.getElementById("locaisMap");
  if (mapContainer !== null) {
    async.include(
      "google-maps",
      "https://maps.googleapis.com/maps/api/js?" + params
    );
  }
};

module.exports = {
  setup: setup
};
