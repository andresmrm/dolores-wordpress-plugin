module.exports = {
  // This must match $break-* definitions in css/scfn/_basic.scss
  breakpoint: {
    tablet: 850,
    desktop: 1150,
    bigDesktop: 1400
  },

  apiBaseURL: "/api/",

  ganalyticsUA: "UA-119107958-1",
  facebookAppID: "403818183426526",
  facebookScope: "public_profile,email",
  googleAppId: "673038519992-dstfvv17h80mgt8eap14nvmmcon85rip" +
               ".apps.googleusercontent.com",
  gmapsKey: "AIzaSyD78Sc-x4ZWybRYSaFn6sY7UI66Vh490ZI",

  strings: {
    authenticatorMessage: "Conecte-se e dê suas ideias para o estado:",
    locationPlaceholder: "Município",
    sharePost: "Que tal compartilhar a ideia com seus amigos?"
  }
};
