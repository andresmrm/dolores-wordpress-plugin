module.exports = {
  // This must match $break-* definitions in css/scfn/_basic.scss
  breakpoint: {
    tablet: 850,
    desktop: 1150,
    bigDesktop: 1400
  },

  apiBaseURL: "/api/",

  ganalyticsUA: "UA-115282129-1",
  facebookAppID: "851000721770498",
  facebookScope: "public_profile,email",
  googleAppId: "166813842188-k3ukboh4klttehr91rm3bboiu0eqs3og" +
               ".apps.googleusercontent.com",
  gmapsKey: "AIzaSyD78Sc-x4ZWybRYSaFn6sY7UI66Vh490ZI",

  strings: {
    authenticatorMessage: "Conecte-se e dê suas ideias para o estado:",
    locationPlaceholder: "Município",
    sharePost: "Que tal compartilhar a ideia com seus amigos?"
  }
};
