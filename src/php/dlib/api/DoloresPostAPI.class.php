<?php
require_once(DOLORES_PATH . '/dlib/posts.php');

require_once(DOLORES_PATH . '/dlib/api/DoloresBaseAPI.class.php');

class DoloresPostAPI extends DoloresBaseAPI {
  function post($request) {

    $post_data = (isset($request['data']) ? $request['data']: null);
    if (!$post_data) {
        $this->_response(400);
    }

    $post = DoloresPosts::add_new_post(
        (isset($post_data['title']) ? $post_data['title']: null),
        (isset($post_data['text']) ? $post_data['text']: null),
        (isset($post_data['tema']) ? $post_data['tema']: null),
        (isset($post_data['place']) ? $post_data['place']: null),
        '' // $request['data']['tags']
    );

    if (is_array($post) && array_key_exists('error', $post)) {
      $this->_error($post['error']);
    }

    return $post;
  }
};
