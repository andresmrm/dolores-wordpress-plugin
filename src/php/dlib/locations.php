<?php
require_once(DOLORES_PATH . '/dlib/assets.php');

class DoloresLocations {
  private static $instance;
  private $locations;

  public static function get_instance() {
    if (null === static::$instance) {
      static::$instance = new static();
    }

    return static::$instance;
  }

  private function __construct() {
    $terms = get_terms('local', array('hide_empty' => false));

    if (count($terms) == 0) {
      $this->setup_locations();
      $terms = get_terms('local', array('hide_empty' => false));
    }

    $this->locations = array();
    foreach ($terms as $location) {
      $location->clean_name = remove_accents($location->name);
      $this->locations[$location->name] = $location;
    }
  }

  public static function region_for_city($city) {
    return get_option('dolores_municipios_regioes')[$city];
  }

  public static function cities_for_region($region) {
      return get_option('dolores_regioes_municipios')[$region];
  }

  public static function get_cities() {
    return get_option('dolores_municipios');
  }

  public static function get_out_of_place_option() {
    return DOLORES_OUT_OF_PLACE_OPTION;
  }

  public function is_valid_city($value) {
    return in_array($value, $this->get_cities());
  }

  public function is_valid_location($value) {
    return array_key_exists($value, $this->locations);
  }

  public function get_suggestions($value) {
    $value = remove_accents($value);
    $suggestions = array();
    foreach ($this->locations as $key => $obj) {
      if (stripos($obj->clean_name, $value) !== FALSE) {
        $suggestions[] = $key;
      }
    }
    return $suggestions;
  }

  public function setup_locations() {
    $data_files = array('municipios.csv');
    foreach ($data_files as $file) {
      $path = 'data/' . DOLORES_TEMPLATE . '/locations/' . $file;
      $asset = DoloresAssets::get_static_path($path);
      if (is_file($asset)) {
        $this->insert_csv($asset);
      }
    }
  }

  public function insert_csv($file) {
    $lines = file($file);
    $cities = array();
    $cities_regions = array();
    $regions_cities = array();
    foreach ($lines as $line) {
      list($region, $city, $lat, $lng) = explode(",", $line);
      array_push($cities, $city);
      $cities_regions[$city] = $region;
      if (!isset($regions_cities[$region])) {
          $regions_cities[$region] = array($city);
      } else {
          array_push($regions_cities[$region], $city);
      }
      $this->insert_location($region, $city, $lat, $lng);
    }
    update_option('dolores_municipios', $cities);
    update_option('dolores_municipios_regioes', $cities_regions);
    update_option('dolores_regioes_municipios', $regions_cities);
  }

  public function insert_location($region, $name, $lat, $lng) {
    $region_term = term_exists($region, 'local');
    if (!$region_term) {
        $region_term = wp_insert_term($region, 'local');
    }
    // $term = wp_insert_term($name, 'local', array('parent'=> $region_term['term_id']));
    // if (is_wp_error($term)) {
    //   return false;
    // }
    if (is_wp_error($region_term)) {
      return false;
    }
    // update_term_meta($term['term_id'], 'lat', $lat);
    // update_term_meta($term['term_id'], 'lng', $lng);
    if (DOLORES_ACTIVE_LOCATION_THRESHOLD == 0) {
      update_term_meta($region_term['term_id'], 'active', 1);
    }
    return true;
  }

  public function get_user_count($location) {
    global $wpdb;

    if (is_string($location)) {
      if (!isset($this->locations[$location])) {
        return 0;
      }
      $location = $this->locations[$location];
    }

    $cities = $this->cities_for_region($location->name);

    $placeholder = join(',', array_fill(0, count($cities), '%s'));

    $sql = <<<SQL
SELECT COUNT(user_id)
  FROM {$wpdb->usermeta}
  WHERE meta_key = 'location' AND meta_value IN ($placeholder)
SQL;

    $query = $wpdb->prepare($sql, $cities);
    return intval($wpdb->get_var($query));
  }

  public function get_missing($location) {
    if (is_string($location)) {
      if (!isset($this->locations[$location])) {
        return 0;
      }
      $location = $this->locations[$location];
    }
    $missing = DOLORES_ACTIVE_LOCATION_THRESHOLD - $this->get_user_count($location);
    if ($missing <= 0) {
      update_term_meta($location->term_id, 'active', 1);
      $missing = 1;
    }
    return $missing;
  }

  public function get_active() {
    return get_terms('local', array(
        'hide_empty' => false,
        'meta_query' => array(
          array(
            'key' => 'active',
            'value' => '1'
          )
        )
    ));
  }

  public function get_ranking() {
    global $wpdb;

    $sql = <<<SQL
SELECT meta_value AS location, COUNT(user_id) AS count
  FROM {$wpdb->usermeta}
  WHERE meta_key = 'location'
  GROUP BY meta_value
  ORDER BY count DESC
SQL;

    return $wpdb->get_results($sql);
  }
};
