<form class="hero-form">
    <div class="hero-form-item">
        <input
            class="hero-form-input"
            type="text"
            name="name"
            id="hero-email"
            placeholder="Nome"
        />
    </div>
    <div class="hero-form-item">
        <input
            class="hero-form-input"
            type="text"
            name="email"
            id="hero-email"
            placeholder="E-mail"
        />
    </div>
    <div class="hero-form-item">
        <input
            class="hero-form-input"
            type="text"
            name="phone"
            id="hero-phone"
            placeholder="Telefone/Whatsapp"
        />
    </div>
    <div class="hero-form-item">
        <select class="hero-form-input" name="location" id="hero-location">
        <?php
            require_once(DOLORES_PATH . '/dlib/locations.php');
            $out_of_place_option = DoloresLocations::get_out_of_place_option();
            echo <<<HTML
<option value="" disabled selected>Escolha sua cidade</option>
<option style="color:black;" value="{$out_of_place_option}">{$out_of_place_option}</option>
HTML;
            $cities = DoloresLocations::get_cities();
            foreach ($cities as $city) {
                echo('<option style="color:black;" value="'.$city.'">'.$city.'</option>');
            }
        ?>
        </select>
    </div>
    <div class="hero-form-item">
        <input type="hidden" name="origin" value="Home" />
        <button class="hero-form-button" type="submit">
        Partiu!
        </button>
    </div>
    <div class="hero-form-response"></div>
</form>
