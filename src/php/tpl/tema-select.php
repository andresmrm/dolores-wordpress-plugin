<p class="tema-form-item">
    <label class="tema-form-label" for="tema-form-tema">
        Tema
    </label>
    <select class="tema-form-select" name="tema" id="tema-form-tema">
        <?php
        $term = get_queried_object();
        $temas = get_terms('tema', array(
            'hide_empty' => false,
            'meta_query' => array(
                array(
                    'key' => 'active',
                    'value' => '1'
                )
            )
        ));
        $options_str = '';
        $tema_selected = false;
        $selected_str = 'selected="selected"';
        foreach ($temas as $tema) {
            $selected = ($term->slug == $tema->slug?$selected_str:'');
            if ($selected) {
                $tema_selected = true;
            }
            $options_str .= sprintf(
                '<option value="%s" %s>%s</option>',
                $tema->slug,
                $selected,
                $tema->name
            );
        }
        echo(sprintf('<option value="" %s>Nenhum específico</option>',
                    !$tema_selected?$selected_str:''));
        echo($options_str);
        ?>
    </select>
</p>
