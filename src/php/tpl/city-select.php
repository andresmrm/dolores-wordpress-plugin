<p class="tema-form-item">
    <label class="tema-form-label" for="tema-form-place">
        Município
    </label>
    <select class="tema-form-select" name="place" id="tema-form-place">
        <option value="" selected="selected">Nenhum específico</option>
        <?php
        require_once(DOLORES_PATH . '/dlib/locations.php');
        $cities = DoloresLocations::get_cities();
        foreach ($cities as $city) {
            echo('<option value="'.$city.'">'.$city.'</option>');
        }
        ?>
    </select>
</p>
