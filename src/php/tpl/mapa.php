<?php
require_once(DOLORES_PATH . '/dlib/locations.php');

the_post();
get_header();
?>

<main class="flow">
  <div class="wrap">
    <h2 class="flow-title">
      <span>Discuta, imagine e mobilize na sua cidade!</span>
    </h2>
    <ol class="flow-list">
      <li class="bairros-flow-item">
        <div class="bairros-flow-item-title-container">
          <h3 class="flow-item-title">
            Etapa #1
          </h3>
        </div>
        <div class="bairros-flow-item-description-container">
          <p class="bairros-flow-item-description">
						Dê ideias para melhorar o sua cidade; leia e opine sobre as ideias
						dos seus vizinhos.
          </p>
        </div>
      </li>
      <li class="bairros-flow-item">
        <div class="bairros-flow-item-title-container">
          <h3 class="flow-item-title">
            Etapa #2
          </h3>
        </div>
        <div class="bairros-flow-item-description-container">
          <p class="bairros-flow-item-description">
						Compartilhe sua ideia com vizinhos em chame mais pessoas para
						participar.
          </p>
        </div>
      </li>
      <li class="bairros-flow-item">
        <div class="bairros-flow-item-title-container">
          <h3 class="flow-item-title">
            Etapa #3
          </h3>
        </div>
        <div class="bairros-flow-item-description-container">
          <p class="bairros-flow-item-description">
            Quer organizar um encontro para discutir sua cidade?
            <a href="/">Entre em contato</a> para te ajudarmos!
          </p>
        </div>
      </li>
      <li class="bairros-flow-item">
        <div class="bairros-flow-item-title-container">
          <h3 class="flow-item-title">
            Etapa #4
          </h3>
        </div>
        <div class="bairros-flow-item-description-container">
          <p class="bairros-flow-item-description">
            Será criado um programa para a sua cidade, a partir das demandas,
            ideias e sugestões de quem vive nela.
          </p>
        </div>
      </li>
    </ol>
  </div>
</main>

<section class="temas-form bg-pattern-light-purple">
  <div class="wrap">

    <?php
    if (is_user_logged_in()) {
        // TODO: list user region posts
        /*
      $user = wp_get_current_user();
      $bairro = get_user_meta($user->ID, 'location', true);
      ?>
      <h2 class="temas-form-title">
        <?php echo $bairro; ?>
      </h2>

      <?php
      $term = get_term_by('name', $bairro, 'local');
      $active = get_term_meta($term->term_id, 'active', true);
      $link = get_term_link($term->term_id, $term->taxonomy);
      if ($active) {
        ?>
        <p class="bairros-form-description">
          <?php
          if ($term->count > 1) {
            echo $term->count; ?>
            ideias já estão sendo debatidas!
            <?php
          } else {
            ?>
            Clique no botão para entrar no debate!
            <?php
          }
          ?>
        </p>

        <p class="bairros-button-container">
          <a class="bairros-button" href="<?php echo $link; ?>">
            Participe
          </a>
        </p>
        <?php
      } else {
        $missing = DoloresLocations::get_instance()->get_missing($term);
        ?>
        <p class="bairros-form-description">
          Faltam <strong><?php echo $missing; ?></strong> pessoas para
          desbloquear seu bairro.
        </p>

        <p class="bairros-button-container">
          <button
              class="bairros-button"
              onclick="FB.ui({method: 'send', link: '<?php
              echo site_url("/mapa/");
              ?>'});">
            Convide seus amigos
          </button>
        </p>
        <?php
      }
        */
    } else {
      ?>
        <!--
      <h2 class="temas-form-title">
        Qual é a sua região?
      </h2>

      <p class="temas-form-description">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      </p>

      <div id="form-bairros"></div>
        -->
      <?php
    }
    ?>
  </div>
</section>

<section class="locais-map-container">
  <div id="locaisMap"></div>
</section>


<?php
$geojson_url = DoloresAssets::get_static_uri(
    'data/' . DOLORES_TEMPLATE . '/locations/regioes.geojson');
// Prepare JSON data about regions
$locations = DoloresLocations::get_instance();
$terms = $locations->get_active();
$regions = array();
foreach ($terms as $term) {
    $name = $term->name;
    $link = get_term_link($term->term_id, $term->taxonomy);
    $user_count = $locations->get_user_count($term->name);
    $post_count = $term->count;
    $regions[$name] = array(
        'user_count' => $user_count,
        'post_count' => $post_count,
        'link' => $link
    );
}
$regions = json_encode(
    $regions, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
?>

<script type="text/javascript">
  window.onload = function() {
    window.addGeoJson('<?php echo($geojson_url)?>')
    window.regions = <?php echo($regions)?>
  };
</script>

<?php
get_footer();
?>
