<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Dolores
 * @subpackage Dolores/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Dolores
 * @subpackage Dolores/public
 * @author     Your Name <email@example.com>
 */
class Dolores_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Dolores_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Dolores_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

        require_once(DOLORES_PATH . '/dlib/assets.php');
		wp_enqueue_style( $this->plugin_name, DoloresAssets::get_theme_uri(DOLORES_TEMPLATE . '/style.css'), array(), $this->version, 'all' );

		// wp_enqueue_style( $this->plugin_name, plugin_dir_url( __DIR__ ) . DOLORES_TEMPLATE . '/style.css', array(), $this->version, 'all' );

		// wp_enqueue_style( $this->plugin_name, plugin_dir_url( __DIR__ ) . DOLORES_TEMPLATE . '/editor.css', array(), $this->version, 'all' );
		// wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/dolores-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Dolores_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Dolores_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

        require_once(DOLORES_PATH . '/dlib/assets.php');
		wp_enqueue_script( $this->plugin_name, DoloresAssets::get_theme_uri(DOLORES_TEMPLATE . '/script.js'), array( 'jquery' ), $this->version, false );

		// wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/dolores-public.js', array( 'jquery' ), $this->version, false );

	}


    function dolores_permalinks () {

        new DoloresPermalinks();

    }


    function set_comment_template($comment_template) {
        global $post;

        if ($post->post_type == 'ideia')
            $comment_template = DOLORES_TEMPLATE_PATH . '/comments-ideia.php';

        return $comment_template;
    }

    function set_single_template($single_template) {
        global $post;

        if ($post->post_type == 'ideia')
            $single_template = DOLORES_TEMPLATE_PATH . '/single-ideia.php';

        return $single_template;
    }

    function set_author_template($author_template) {
        return DOLORES_TEMPLATE_PATH . '/author.php';
    }

    function set_template( $template ){
        global $post;

        if( is_tax('tema') )
            $template = DOLORES_TEMPLATE_PATH . '/taxonomy-tema.php';
        elseif( is_tax('local') )
            $template = DOLORES_TEMPLATE_PATH . '/taxonomy-local.php';
        elseif( $post && array_key_exists('post_name', $post) ) {
            if( 'temas' == $post->post_name )
                // Listagem de Temas
                $template = DOLORES_TEMPLATE_PATH . '/temas.php';
            elseif( 'mapa' == $post->post_name )
                // Mapa
                $template = DOLORES_TEMPLATE_PATH . '/mapa.php';
            elseif( 'api' == $post->post_name ) {
                // API
                assert(!headers_sent());
                Header('Content-Type: application/json; charset=utf-8');

                require_once(DOLORES_PATH . '/dlib/api/routes.php');

                if (array_key_exists('route', $_REQUEST)) {
                    $route = $_REQUEST['route'];
                } else {
                    Header('HTTP/1.0 404');
                    echo json_encode(Array('error' => 'Request missing route'));
                    exit();
                }

                if (array_key_exists($route, $DOLORES_ROUTES)) {
                    $endpoint = $DOLORES_ROUTES[$route];
                } else {
                    Header('HTTP/1.0 404');
                    echo json_encode(Array('error' => 'Invalid route'));
                    exit();
                }

                $API = new $endpoint($_SERVER['REQUEST_METHOD']);
                echo $API->process();
                try {
                    $API = new $endpoint($_SERVER['REQUEST_METHOD']);
                    echo $API->process();
                } catch (Exception $e) {
                    Header('HTTP/1.0 500');
                    echo json_encode(Array('error' => $e->getMessage()));
                    exit();
                }
            }
        }


        return $template;
    }

    function add_to_footer() {
        require_once(DOLORES_PATH . '/dlib/locations.php');

        $cities = json_encode(DoloresLocations::get_cities());
        $out_of_place_option = DoloresLocations::get_out_of_place_option();

        echo <<<HTML
<script>
window.cities = {$cities}
window.dolores_out_of_place_option = "{$out_of_place_option}"
</script>
<div id="authenticator"></div>
<div id="share-container"></div>
<div id="message-container"></div>
HTML;
    }

    function add_user_button_to_menu($nav) {
        require_once(DOLORES_PATH . '/dlib/users.php');
        // return $nav . DoloresUsers::getUserHeaderLi();
        return preg_replace('@<a.*>#login#</a>@', DoloresUsers::getUserHeaderLi(), $nav);
    }

    function ideia_published_notification($ID, $post) {
        require_once(DOLORES_PATH . '/dlib/mailer.php');
        $author = $post->post_author; /* Post author ID. */
        $name = get_the_author_meta( 'display_name', $author );
        $email = get_the_author_meta( 'user_email', $author );
        $title = $post->post_title;
        $permalink = get_permalink( $ID );
        // $edit = get_edit_post_link( $ID, '' );
        $to[] = sprintf( '%s <%s>', $name, $email );
        // $subject = sprintf( 'Published: %s', $title );
        // $message = sprintf ('Congratulations, %s! Your article “%s” has been published.' . "\n\n", $name, $title );
        // $message .= sprintf( 'View: %s', $permalink );
        // $headers[] = '';
        // wp_mail( $to, $subject, $message, $headers );

        $args = array(
            'NAME' => $name,
            'IDEIA' => htmlspecialchars($title),
            'LINK' => $permalink
        );
        dolores_mail($to, 'ideia_published.html', $args);

    }
}
